angular.module('app').controller('cardCtrl', ['cardFactory', function (cardFactory) {
    this.removeCard = function (card) {
        cardFactory.removeCard(card)
    };

    this.isEditCard = false;
    this.editingCard = null;

    this.editCard = function (card) {
        this.isEditCard = true;
        this.editingCard = angular.copy(card);
    };

    this.updateCard = function () {
        cardFactory.updateCard(this.editingCard);
        this.editingCard = null;
        this.isEditCard = false;
    };

}]);