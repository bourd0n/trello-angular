angular.module('app').controller('listsCtrl', ['listsFactory', function (listsFactory) {
    console.log('listsCtrl');
    this.lists = listsFactory.getLists();

    this.addList = function() {
        listsFactory.addList(this.listName);
        this.listName = '';
    }
}]);