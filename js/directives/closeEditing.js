angular.module('app').directive('closeEditing', function () {
    var KEYS = {
        ESCAPE : 27
    };
    return {
        scope: {
            isEditCard: "="
        },
        link: function (scope, element, attrs) {
            console.log('LINK', scope.isEditCard);
            element.on('keyup', function (e) {
               if (_.isEqual(e.keyCode, KEYS.ESCAPE)) {
                   scope.isEditCard = false;
                   scope.$apply();
               }
            });
        }
    };
});